%% This is formulae, a package for typesetting formulas of the
%% propositional calculus.
%% 
%% Copyright (C) 2011 by Hermogenes Oliveira
%% 
%% This file may be distributed under the terms of the LaTeX Project
%% Public License, as described in lppl.txt in the base LaTeX
%% distribution.  Either version 1 or, at your option, any later
%% version. 
%% ======================= Indentification ===========================
\ProvidesPackage{formulae}[2009/11/05 A package for typesetting
  formulas of the propositional calculus]%
\NeedsTeXFormat{LaTeX2e}[1994/06/01]%

%% ================== Preliminary Declarations =======================
\RequirePackage{ifthen}%
\RequirePackage{begriff}%

%% =========================== Options ===============================

%% Globally typeset formulas in Frege's Begriffsschrift notation
\DeclareOption{begriffsschrift}{%
  \setlength{\BGthickness}{0.5pt}
  \def\formulae@Notation{begriffsschrift}%
  \def\cond#1#2{\ensuremath{\BGconditional{#1}{#2}}}%
  \def\conj#1#2{\ensuremath{\BGnot\BGconditional{#1}{%
      \BGnot{#2}}}}%
  \def\disj#1#2{\ensuremath{\BGconditional{\BGnot#1}{#2}}}%
  \def\bicond#1#2{\conj{\cond{#1}{#2}}{%
      \cond{#2}{#1}}}%
  \def\negt#1{\ensuremath{\BGnot#1}}%
}%

%% Globally typeset formulas in Lukasiewicz's prefix notation
\DeclareOption{lukasiewicz}{%
  \def\formulae@Notation{lukasiewicz}%
  \def\cond#1#2{\lukasiewicz@cond{}#1#2}%
  \def\conj#1#2{\lukasiewicz@conj{}#1#2}%
  \def\disj#1#2{\lukasiewicz@disj{}#1#2}%
  \def\bicond#1#2{\lukasiewicz@bicond{}#1#2}%
  \def\negt#1{\lukasiewicz@negt{}#1}%
}%

%% Globally typeset formulas in the standard infix notation with the
%% more commonly used symbols for connectives.
\DeclareOption{standard}{%
  \def\formulae@Notation{standard}%
  \def\cond#1#2{\ensuremath{#1\mathbin{\standard@cond}#2}}%
  \def\conj#1#2{\ensuremath{#1\mathbin{\standard@conj}#2}}%
  \def\disj#1#2{\ensuremath{#1\mathbin{\standard@disj}#2}}%
  \def\bicond#1#2{\ensuremath{#1\mathbin{\standard@bicond}#2}}%
  \def\negt#1{\ensuremath{\mathord{\standard@negt}#1}}%
}%

\DeclareOption*{%
  \PackageWarning{formulae}{Unknown option `\CurrentOption'}%
}%

\ExecuteOptions{standard}%
\ProcessOptions*
%% ========================== Main Code ==============================

\def\standard@cond{\to}
\def\standard@conj{\land}
\def\standard@disj{\lor}
\def\standard@bicond{\leftrightarrow}
\def\standard@negt{\lnot}


\def\lukasiewicz@cond{C}
\def\lukasiewicz@conj{K}
\def\lukasiewicz@disj{A}
\def\lukasiewicz@bicond{E}
\def\lukasiewicz@negt{N}

%% Provides a command for the user to change the symbol for a
%% particular logical constant in Lukasiewicz's and the standard
%% notations

\newcommand{\redefinesymbol}[3]{%
  \ifcase#1%
  \ifcase#2%
  \def\standard@cond{#3}\or%
  \def\standard@conj{#3}\or%
  \def\standard@disj{#3}\or%
  \def\standard@bicond{#3}\or%
  \def\standard@negt{#3}\fi
  \or%
  \ifcase#2%
  \def\lukasiewicz@cond{#3}\or%
  \def\lukasiewicz@conj{#3}\or%
  \def\lukasiewicz@disj{#3}\or%
  \def\lukasiewicz@bicond{#3}\or%
  \def\lukasiewicz@negt{#3}\fi
  \fi
}

%% Remove the outer parentheses in the Standard notation.  This is
%% done via the \ifroot TeX boolean.  Also helps to control the use of
%% parentheses in negated formulas via the \ifpos TeX boolean.
\def\formulae@Parentheses#1{\postrue\ifroot\rootfalse%
  \ensuremath{#1}\else \ensuremath{(#1)}\fi}

%% (Re)defines commands for the standard notation
\def\formulae@SetStandard{%
  \def\cond##1##2{\formulae@Parentheses{##1%
      \mathbin{\standard@cond}##2}}%
  \def\conj##1##2{\formulae@Parentheses{##1\mathbin{\standard@conj}##2}}%
  \def\disj##1##2{\formulae@Parentheses{##1\mathbin{\standard@disj}##2}}%
  \def\bicond##1##2{\formulae@Parentheses{##1%
      \mathbin{\standard@bicond}##2}}%
  \def\negt##1{\rootfalse\ifpos\mathord{\standard@negt}##1\else%
    \mathord{\standard@negt}\formulae@Parentheses{##1}\fi}%
}%

%% (Re)defines commands for Lukasiewicz's prefix notation
\newcommand{\formulae@SetLukasiewicz}{%
  \def\cond##1##2{\lukasiewicz@cond{}##1##2}%
  \def\conj##1##2{\lukasiewicz@conj{}##1##2}%
  \def\disj##1##2{\lukasiewicz@disj{}##1##2}%
  \def\bicond##1##2{\lukasiewicz@bicond{}##1##2}%
  \def\negt##1{\lukasiewicz@negt{}##1}%
}%

%% (Re)defines commands for Frege's Begriffsschrift notation
\newcommand{\formulae@SetBegriffsschrift}{%
  \setlength{\BGthickness}{0.5pt}
  \def\cond##1##2{\BGconditional{##1}{##2}}%
  \def\conj##1##2{\BGnot\BGconditional{##1}{%
      \BGnot{##2}}}%
  \def\disj##1##2{\BGconditional{\BGnot##1}{##2}}%
  \def\bicond##1##2{\conj{\cond{##1}{##2}}{%
      \cond{##2}{##1}}}%
  \def\negt##1{\BGnot##1}%
}%

%% This command is the main command provided to the user
\newcommand{\formula}[2][\formulae@Notation]{%
  \ifthenelse{\equal{#1}{lukasiewicz}}{\formulae@SetLukasiewicz #2}{%
  \ifthenelse{\equal{#1}{standard}}{%
    %% TeX booleans to handle parentheses
    \begingroup\newif\ifroot\roottrue%
    \newif\ifpos\postrue%
    \formulae@SetStandard\ensuremath{#2}\endgroup}{%
  \ifthenelse{\equal{#1}{begriffsschrift}}{%
  \formulae@SetBegriffsschrift\ensuremath{\BGassert #2}}{}}}%
}%

%% =================== End of file formulae.sty ======================
